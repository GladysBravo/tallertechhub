const express = require('express');
const morgan = require("morgan");

const app = express();

app.use(morgan("dev"));

app.use((req, res, next) => {
    console.log("Ingresando petición...: ", req.url);
    next();
});

app.get('*', ((req, res) => {   
    console.log("Ingresando a cualquier petición GET.");
    res.writeHead(200, {
        'Content-Type': 'text/plain',
    })
    res.end("\n*** Valor devuelto*** \n");
}));

app.listen(4000);