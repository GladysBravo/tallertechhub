# Taller Tech Hub 

Ejemplo para taller	(ExpressJS)

## Requisitos 

Debe tener instalado lo siguiente:

> NodeJS


## Descargar el proyecto

```sh
$ git clone <url del proyecto>
```


## Instalar las librerías necesarias

Si se está utilizando nvm, asegurarse que se está usando la versión de node correcta.

```sh
$ nvm use 8.9.4
```

Instalar las librerías
```sh
$ npm install
```
